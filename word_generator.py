#!/usr/bin/python
# -*- coding: utf-8 -*-

from docx import Document
from num2t4ru import num2text
from datetime import datetime
from os import path
from tqdm import tqdm
import pandas

BASEDOC_PATH = 'Pretenziya_empty.docx'
EXCEL_PATH = 'DataTable.xls'
RESULT_PATH = 'results'
DATE_NOW = ('ДАТА_СЕГ', datetime.now().date().strftime('%d.%m.%Y'))


def _values_to_text(elementdict):
    resultdict = {}
    for key, val in elementdict.iteritems():
        resultdict[key] = num2text(val)
    return resultdict


def _deeplen(datadict):
    test_key = datadict.keys()[0]
    return len(datadict[test_key])


def convert_from_excel(excel_path):
    postprocess_tags = ('ДОЛГ', 'ОПЛАТА')
    xls = pandas.ExcelFile(excel_path)
    datadict = xls.parse(xls.sheet_names[0]).to_dict()
    for tag in postprocess_tags:
        datadict[unicode(tag + '_ТХТ')] = _values_to_text(datadict[unicode(tag + '1')])
    return datadict


def replace_document(datadict):
    for docnumber in tqdm(xrange(_deeplen(datadict))):
        res_path = path.join(RESULT_PATH, 'Doc{}.docx'.format(docnumber))
        document = Document(BASEDOC_PATH)
        for paragraph in document.paragraphs:
            if not paragraph.text:
                continue
            selected_tags = [conc_tag for conc_tag in datadict.iterkeys() if paragraph.text.find(conc_tag) > 0]
            date_found = paragraph.text.find(DATE_NOW[0]) > 0
            for styled_fragment in paragraph.runs:
                for tag in selected_tags:
                    styled_fragment.text = styled_fragment.text.replace(tag, unicode(datadict[tag][docnumber]))
                if date_found:
                    styled_fragment.text = styled_fragment.text.replace(DATE_NOW[0], DATE_NOW[1])
        document.save(res_path)


def main():
    replace_document(convert_from_excel(EXCEL_PATH))


if __name__ == '__main__':
    main()